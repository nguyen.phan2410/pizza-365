$(document).ready(function () {
	"use strict";
	/*** REGION 1 - Global variables */
	var gSelectedMenu = {
		kichCo: "",
		duongKinh: "",
		suon: -1,
		salad: "",
		soLuongNuoc: -1,
		thanhTien: -1,
	};

	var gSelectedPizza = "";

	var gOrderObj = {};

	/*** REGION 2 - Elements' events declaration */
	//add events to menu btns
	$("#btn-small").on("click", function () {
		onBtnSmallClick();
	});
	$("#btn-medium").on("click", function () {
		onBtnMediumClick();
	});
	$("#btn-large").on("click", function () {
		onBtnLargeClick();
	});

	//add events to pizza btns
	$("#btn-seafood").on("click", function () {
		onBtnSeafoodClick();
	});
	$("#btn-hawaii").on("click", function () {
		onBtnHawaiiClick();
	});
	$("#btn-bacon").on("click", function () {
		onBtnBaconClick();
	});

	//add event to send btn
	$("#btn-send").on("click", function () {
		onBtnSendClick();
	});

	//form submit
	$("form").on("submit", function (e) {
		e.preventDefault();
	});

	//add event to Confirm btn
	$("#btn-confirm").on("click", function () {
		onBtnConfirmClick();
	});

	onPageLoading();
	/*** REGION 3 - Event handlers */
	//runs when Confirm btn is clicked
	function onBtnConfirmClick() {
		callAPICreateOrder(gOrderObj);
	}

	//runs when Send btn is clicked
	function onBtnSendClick() {
		//get the order
		gOrderObj = getOrder();
		//validate order
		var vValid = validateOrder(gOrderObj);
		if (vValid) {
			if (gOrderObj.idVourcher != "") {
				callAPIGetDiscount(gOrderObj.idVourcher);
			} else {
				showOrder(gOrderObj, "");
			}
		}
	}

	//runs when page load
	function onPageLoading() {
		callAPIGetDrinkList();
	}
	//runs when Small Btn is clicked
	function onBtnSmallClick() {
		changeSelectedMenu("S");
	}

	//runs when Medium Btn is clicked
	function onBtnMediumClick() {
		changeSelectedMenu("M");
	}

	//runs when Large Btn is clicked
	function onBtnLargeClick() {
		changeSelectedMenu("L");
	}

	//runs when Seafood Btn is clicked
	function onBtnSeafoodClick() {
		changeSelectedPizza("SEAFOOD");
	}

	//runs when Hawaii Btn is clicked
	function onBtnHawaiiClick() {
		changeSelectedPizza("HAWAII");
	}

	//runs when Bacon Btn is clicked
	function onBtnBaconClick() {
		changeSelectedPizza("BACON");
	}
	/*** REGION 4 - Common funtions */
	//call api create order
	//input: order obj
	function callAPICreateOrder(paramOrderObj) {
		$.ajax({
			type: "POST",
			url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
			data: JSON.stringify(paramOrderObj),
			contentType: "application/json;charset=UTF-8",
			success: function (paramRes) {
				$("#modal-order-detail").modal("hide");
				$("#inp-order-code").val(paramRes.orderCode);
				$("#modal-order-code").modal("show");
			},
			error: function () {
				alert("Không tạo được order");
			},
		});
	}

	//call api get discount
	//input: id voucher
	function callAPIGetDiscount(paramIdVoucher) {
		$.ajax({
			type: "GET",
			url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail" + "/" + paramIdVoucher,
			success: function (paramRes) {
				showOrder(gOrderObj, paramRes);
			},
			error: function () {
				showOrder(gOrderObj, "");
			},
		});
	}

	//show order on modal
	//input: order obj
	function showOrder(paramOrderObj, paramDiscount) {
		$("#inp-order-ho-ten").val(paramOrderObj.hoTen);
		$("#inp-order-so-dien-thoai").val(paramOrderObj.soDienThoai);
		$("#inp-order-dia-chi").val(paramOrderObj.diaChi);
		$("#inp-order-loi-nhan").val(paramOrderObj.loiNhan);
		$("#inp-order-giam-gia").val(paramOrderObj.idVourcher);

		var vOrderDetail =
			"Xác nhận: " +
			paramOrderObj.hoTen +
			"," +
			paramOrderObj.soDienThoai +
			"," +
			paramOrderObj.diaChi +
			"\r\n" +
			"Menu: " +
			paramOrderObj.kichCo +
			",suon: " +
			paramOrderObj.suon +
			",nước: " +
			paramOrderObj.soLuongNuoc +
			",loại nước: " +
			paramOrderObj.idLoaiNuocUong +
			",đường kính: " +
			paramOrderObj.duongKinh +
			",salad: " +
			paramOrderObj.salad +
			"\r\n" +
			"Loại pizza: " +
			paramOrderObj.loaiPizza +
			",Giá: " +
			paramOrderObj.thanhTien +
			",Mã giảm giá: " +
			paramOrderObj.idVourcher +
			"\r\n" +
			"Phải thanh toán: ";

		if (paramDiscount == "") {
			vOrderDetail += paramOrderObj.thanhTien + " vnd";
		} else {
			vOrderDetail += (paramOrderObj.thanhTien * (100 - paramDiscount.phanTramGiamGia)) / 100 + " vnd" + "(giảm giá " + paramDiscount.phanTramGiamGia + "%)";
		}

		$("#inp-order-thong-tin").html(vOrderDetail);
		$("#modal-order-detail").modal("show");
	}

	//validate order
	//input: order obj
	//return: true/false
	function validateOrder(paramOrderObj) {
		if (paramOrderObj.kichCo == "") {
			alert("Chưa chọn menu");
			return false;
		}

		if (paramOrderObj.loaiPizza == "") {
			alert("Chưa chọn pizza");
			return false;
		}

		if (paramOrderObj.idLoaiNuocUong == "") {
			alert("Chưa chọn nước uống");
			return false;
		}

		if (paramOrderObj.hoTen == "") {
			alert("Chưa nhập họ tên");
			return false;
		}

		if (paramOrderObj.email == "") {
			alert("Chưa nhập email");
			return false;
		}

		if (!validateEmail(paramOrderObj.email)) {
			alert("Nhập lại email");
			return false;
		}

		if (paramOrderObj.soDienThoai == "") {
			alert("Chưa nhập số điện thoại");
			return false;
		}
		if (paramOrderObj.diaChi == "") {
			alert("Chưa nhập địa chỉ");
			return false;
		}

		return true;
	}

	//validate email
	//input: email
	//output: true/false
	function validateEmail(paramEmail) {
		var vRegrex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return paramEmail.match(vRegrex);
	}

	//get order
	//return: order obj
	function getOrder() {
		var vOrderObj = {
			kichCo: gSelectedMenu.kichCo,
			duongKinh: gSelectedMenu.duongKinh,
			suon: gSelectedMenu.suon,
			salad: gSelectedMenu.salad,
			soLuongNuoc: gSelectedMenu.soLuongNuoc,
			thanhTien: gSelectedMenu.thanhTien,
			loaiPizza: gSelectedPizza,
			idVourcher: $("#inp-giam-gia").val().trim(),
			idLoaiNuocUong: $("#sel-drink").val(),
			hoTen: $("#inp-ho-ten").val().trim(),
			email: $("#inp-email").val().trim(),
			soDienThoai: $("#inp-so-dien-thoai").val().trim(),
			diaChi: $("#inp-dia-chi").val().trim(),
			loiNhan: $("#inp-loi-nhan").val().trim(),
		};
		return vOrderObj;
	}

	//change menu btns color and selected attr
	//input: "S", "M", "L"
	function changeSelectedMenu(paramMenu) {
		//all the menu btns
		var vBtnSmall = $("#btn-small");
		var vBtnMedium = $("#btn-medium");
		var vBtnLarge = $("#btn-large");
		//change menu btns color and attr
		if (paramMenu == "S") {
			vBtnSmall.attr("class", "btn btn-info w-100");

			vBtnMedium.attr("class", "btn btn-warning w-100");

			vBtnLarge.attr("class", "btn btn-warning w-100");

			getMenu("S", "20cm", 2, "200g", 2, 150000);
		}

		if (paramMenu == "M") {
			vBtnSmall.attr("class", "btn btn-warning w-100");

			vBtnMedium.attr("class", "btn btn-info w-100");

			vBtnLarge.attr("class", "btn btn-warning w-100");

			getMenu("M", "25cm", 4, "300g", 3, 200000);
		}

		if (paramMenu == "L") {
			vBtnSmall.attr("class", "btn btn-warning w-100");

			vBtnMedium.attr("class", "btn btn-warning w-100");

			vBtnLarge.attr("class", "btn btn-info w-100");

			getMenu("L", "30cm", 8, "500g", 4, 250000);
		}

		console.log(gSelectedMenu);
	}

	//get menu obj
	//input: all the data of the menu
	function getMenu(paramKichCo, paramDuongKinh, paramSuon, paramSalad, paramSoLuongNuoc, paramThanhTien) {
		gSelectedMenu.kichCo = paramKichCo;
		gSelectedMenu.duongKinh = paramDuongKinh;
		gSelectedMenu.suon = paramSuon;
		gSelectedMenu.salad = paramSalad;
		gSelectedMenu.soLuongNuoc = paramSoLuongNuoc;
		gSelectedMenu.thanhTien = paramThanhTien;
	}

	//change pizza btns color and selected attr
	//input: "seafood", "hawaii", "bacon"
	function changeSelectedPizza(paramPizza) {
		var vBtnSeafood = $("#btn-seafood");
		var vBtnHawaii = $("#btn-hawaii");
		var vBtnBacon = $("#btn-bacon");
		//change menu btns color and attr
		if (paramPizza == "SEAFOOD") {
			vBtnSeafood.attr("class", "btn btn-info w-100");

			vBtnHawaii.attr("class", "btn btn-warning w-100");

			vBtnBacon.attr("class", "btn btn-warning w-100");
		}

		if (paramPizza == "HAWAII") {
			vBtnSeafood.attr("class", "btn btn-warning w-100");

			vBtnHawaii.attr("class", "btn btn-info w-100");

			vBtnBacon.attr("class", "btn btn-warning w-100");
		}

		if (paramPizza == "BACON") {
			vBtnSeafood.attr("class", "btn btn-warning w-100");

			vBtnHawaii.attr("class", "btn btn-warning w-100");

			vBtnBacon.attr("class", "btn btn-info w-100");
		}
		gSelectedPizza = paramPizza;
		console.log(gSelectedPizza);
	}

	//call api to get drink list
	function callAPIGetDrinkList() {
		$.ajax({
			type: "GET",
			url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
			success: function (paramRes) {
				loadDrinkListToSelect(paramRes);
			},
			error: function () {
				alert("Không lấy được danh sách đồ uống");
			},
		});
	}

	//load drink list to select
	//input: array of drinks
	function loadDrinkListToSelect(paramDrinkList) {
		$.each(paramDrinkList, function (i, drink) {
			$("#sel-drink").append(
				$("<option>", {
					text: drink.tenNuocUong,
					value: drink.maNuocUong,
				})
			);
		});
	}
});
